package objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A class that contains a number of squares either hidden as one or to make a
 * larger object.
 * 
 * @author Rick
 * 
 */

public class MultipleBlockEnemy implements Serializable {

	private static final long serialVersionUID = 1L;
	private ArrayList<Enemy> enemies;
	/**
	 * Used to tell if the object is visibly split into different movement
	 * patterns.
	 */
	private boolean split;
	/**
	 * Used to tell if the object's parts all still exists.
	 */
	private boolean exists;

	public MultipleBlockEnemy() {
		enemies = new ArrayList<Enemy>();
	}

	public boolean getExists() {
		return exists;
	}

	public void setExists(boolean exists) {
		this.exists = exists;
	}

	public void addEnemy(Enemy e) {
		enemies.add(e);
	}

	public void clearEnemies() {
		enemies.clear();
	}

	public void removeEnemy(Enemy e) {
		enemies.remove(e);
	}

	public ArrayList<Enemy> getEnemies() {
		return enemies;
	}

	public boolean isSplit() {
		return split;
	}

	public void setSplit(boolean s) {
		split = s;
	}

}
