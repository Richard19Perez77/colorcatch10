package radical.appwards.colorcatch;

import game.Game;
import game.GameFactory;
import variables.GameVariables;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * The class that creates the view to be seen and the thread to update the view
 * as well as draw the objects to be shown. Most important for the game logic is
 * the doDraw and updatePhysics methods for the Game, as well as the onTouch
 * event handling.
 *
 * @author Rick Perez
 *
 */
public class ColorCatchView extends SurfaceView implements SurfaceHolder.Callback {

	/**
	 * Used to display messages to the user when the application is paused.
	 */
	private TextView mStatusText;
	/**
	 * Used to start the application and is usually hidden during gameplay.
	 */
	private Button mStartButton;
	/**
	 * Create to run the draw and update methods.
	 */
	private GameThread thread;
	/**
	 * Used to create a game. If expanded the factory can create more than one
	 * game in a menu if programmed to.
	 */
	private GameFactory gf;

	/**
	 * The game logic of Color Catch.
	 */
	private Game colorCatch;

	/**
	 * allows for the creation of color catch game in the game factory
	 */
	private final int COLOR_CATCH = 1;

	/***
	 * allows for saving of variables across classes
	 */
	GameVariables gv;

	/**
	 * Creates the View the user will see.
	 *
	 * @param context
	 *            Contains the state of the game.
	 * @param attrs
	 */
	public ColorCatchView(Context context, AttributeSet attrs) {
		super(context, attrs);
		// before making the game touch ready I load the intro screen
		setFocusable(false);

		// basically, allows the canvas to be drawn on
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);

		gv = GameVariables.getInstance();

		// with the context we can add messages to the view
		gv.context = context;

		// the start of the app is paused with a message to unpause to start
		thread = new GameThread(holder, gv.context, new Handler() {
			@Override
			public void handleMessage(Message m) {
                int viz = m.getData().getInt("viz");
                if(viz == 0)
                    mStatusText.setVisibility(View.VISIBLE);
                else
                    mStatusText.setVisibility(View.INVISIBLE);

				mStatusText.setText(m.getData().getString("text"));
			}
		});

		// while paused this instant is enough to start loading the app
		gf = new GameFactory();
		colorCatch = gf.createGame(COLOR_CATCH);

		// initializes the screen variables but they may change is we remove the
		// status bar which i will do but for now its good enough to size the
		// splash screen. DisplayMetrics contains info about the phone.
		// Resources
		// like strings, draw able and raw data files can be accessed now.
		colorCatch.init(getResources().getDisplayMetrics(),
				context.getResources());

		// now we can begin to accept touch events, but we might need to load
		// other variables specific to the game.
		setFocusable(true); // make sure we get key events
	}

	/**
	 * Used to get the thread, mostly from the activity class so we can pause
	 * and resume the thread when the phone's home or back button is pressed.
	 *
	 * @return The game thread.
	 */
	public GameThread getThread() {
		return thread;
	}

	/**
	 * When the start button is pressed its heard first in the Activity class
	 * and then this method is called.
	 */
	public void startGame() {
		colorCatch.startGame();
		removeStartButton();
		thread.unpause();
	}

	public void showStartButton() {
		mStartButton.setVisibility(VISIBLE);
	}

	public void removeStartButton() {
		mStartButton.setVisibility(INVISIBLE);
	}

	/**
	 * Random events that close the app or steal the view will call this and
	 * pause the game.
	 */
	@Override
	public void onWindowFocusChanged(boolean hasWindowFocus) {
		if (!hasWindowFocus)
			thread.pause();
	}

	/**
	 * A reference to the text box created in the activity class.
	 *
	 * @param textView
	 *            The text box to be shown.
	 */
	public void setTextView(TextView textView) {
		mStatusText = textView;
	}

	/**
	 * A reference to the start button created in the activity class.
	 *
	 * @param button
	 *            The button to be shown.
	 */
	public void setButtonView(Button button) {
		mStartButton = button;
	}

	/**
	 * Callback invoked when the surface dimensions change. I have locked the
	 * game as portrait in the manifest.xml so it won't be used.
	 */
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
							   int height) {
		thread.setSurfaceSize(width, height);
	}

	/**
	 * Callback invoked when the Surface has been created and is ready to be
	 * used. If the thread is destroyed or is killed for some reason we need to
	 * create a new one or the game will crash. SetRunning allows the code to be
	 * run. Starting the thread runs it.
	 */
	public void surfaceCreated(SurfaceHolder holder) {
		// start the thread here so that we don't busy-wait in run()
		// waiting for the surface to be created
		if (thread == null || thread.getState() == Thread.State.TERMINATED) {
			thread = new GameThread(holder, gv.context, new Handler() {
				@Override
				public void handleMessage(Message m) {
					int viz = m.getData().getInt("viz");
					if(viz == 0)
						mStatusText.setVisibility(View.VISIBLE);
					else
						mStatusText.setVisibility(View.INVISIBLE);

					mStatusText.setText(m.getData().getString("text"));
				}
			});
		}

		thread.setRunning(true);
		thread.start();
	}

	/**
	 * When the application is closing we can end the current thread.
	 */
	public void surfaceDestroyed(SurfaceHolder holder) {
		boolean retry = true;
		thread.setRunning(false);
		while (retry) {
			try {
				thread.join();
				retry = false;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Pauses the game thread.
	 */
	public void pause() {
		thread.pause();
	}

	/**
	 * Resumes the game thread.
	 */
	public void resume() {
		thread.unpause();
	}

	public boolean performClick() {
		return super.performClick();
	}

	/**
	 * Here is the screen touch event logic. The event carried the coordinates
	 * and the type of gesture made. I pass this to the color catch game to
	 * handle the events if the game is started. If its the loading screen it
	 * moves the game to the menu screen. If in the menu screen the only thing
	 * else it can do is unpause it and wait for the start button to be pressed.
	 * Odd situations where the game has paused from actual phone events, the
	 * game will unpause.
	 */
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		synchronized (getThread().getSurfaceHolder()) {
			performClick();
			if (mStatusText.isShown()) {
				mStatusText.setVisibility(INVISIBLE);

				if (colorCatch.getLoadingPlaying()) {
					// go from loading screen to menu screen
					colorCatch.setMenuScreen();
					showStartButton();
					thread.unpause();
				} else if (colorCatch.getMenuPlaying()) {
					// if in menu screen show start button
					showStartButton();
					thread.unpause();
				} else {
					thread.unpause();
				}

				return false;
			} else if (colorCatch.getGameVarsLoaded()) {
				// store touch point and do some action
				colorCatch.setNewOnTouchCoords((int) event.getX(),
						(int) event.getY());
				colorCatch.eventAction(event);
			}
			return true;
		}

	}

	/**
	 * A Class to contain the thread that will be used to run the game's logic.
	 *
	 * @author Rick
	 *
	 */
	class GameThread extends Thread {

		public static final int STATE_LOSE = 1;
		public static final int STATE_PAUSE = 2;
		public static final int STATE_READY = 3;
		public static final int STATE_RUNNING = 4;
		public static final int STATE_WIN = 5;

		/**
		 * I don't use difficult but we could modify the game to add more
		 * squares or speed.
		 */
		private static final String KEY_DIFFICULTY = "mDifficulty";

		private int mDifficulty;

		/** used to tell if the game is paused or running. */
		private int mMode;

		private boolean mRun = false;

		private SurfaceHolder mSurfaceHolder;

		private Handler mHandler;

		public GameThread(SurfaceHolder surfaceHolder, Context context,
						  Handler handler) {
			// get handles to some important objects
			mSurfaceHolder = surfaceHolder;
			mHandler = handler;
			gv.context = context;
		}

		public void doStart() {
			synchronized (mSurfaceHolder) {
				setState(STATE_RUNNING);
			}
		}

		public void pause() {
			synchronized (mSurfaceHolder) {
				if (mMode == STATE_RUNNING) {
					setState(STATE_PAUSE);
				}
			}
		}

		public void unpause() {
			synchronized (mSurfaceHolder) {
				setState(STATE_RUNNING);
			}
		}

		public synchronized void restoreState(Bundle savedState) {
			synchronized (mSurfaceHolder) {
				setState(STATE_PAUSE);
				mDifficulty = savedState.getInt(KEY_DIFFICULTY);
			}
		}

		/**
		 * Contains the calls to update the GamePhysics and then draw the screen
		 * objects.
		 */
		@Override
		public void run() {
			while (mRun) {
				Canvas c = null;
				try {
					c = mSurfaceHolder.lockCanvas(null);
					synchronized (mSurfaceHolder) {
						if (mMode == STATE_RUNNING)
							updatePhysics();
						if (c != null)
							doDraw(c);
					}
				} finally {
					if (c != null) {
						mSurfaceHolder.unlockCanvasAndPost(c);
					}
				}
			}
		}

		public Bundle saveState(Bundle map) {
			synchronized (mSurfaceHolder) {
				if (map != null) {
					map.putInt(KEY_DIFFICULTY, Integer.valueOf(mDifficulty));
				}
			}
			return map;
		}

		public void setDifficulty(int difficulty) {
			synchronized (mSurfaceHolder) {
				mDifficulty = difficulty;
			}
		}

		public void setRunning(boolean b) {
			synchronized (mSurfaceHolder) {
				mRun = b;
			}
		}

		public void setState(int mode) {
			synchronized (mSurfaceHolder) {
				setState(mode, null);
			}
		}

		public void setState(int mode, CharSequence message) {
			synchronized (mSurfaceHolder) {
				mMode = mode;
				if (mMode == STATE_RUNNING) {
					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putString("text", "");
					b.putInt("viz", View.INVISIBLE);
					msg.setData(b);
					mHandler.sendMessage(msg);
				} else {
					Resources res = gv.context.getResources();
					CharSequence str = "";
					if (mMode == STATE_READY)
						str = res.getText(R.string.mode_ready);
					else if (mMode == STATE_PAUSE)
						str = res.getText(R.string.mode_pause);
					else if (mMode == STATE_LOSE)
						str = res.getText(R.string.mode_lose);
					else if (mMode == STATE_WIN)
						str = res.getString(R.string.mode_win_prefix)
								+ res.getString(R.string.mode_win_suffix);

					if (message != null) {
						str = message + "\n" + str;
					}

					Message msg = mHandler.obtainMessage();
					Bundle b = new Bundle();
					b.putString("text", str.toString());
					b.putInt("viz", View.VISIBLE);
					msg.setData(b);
					mHandler.sendMessage(msg);
				}
			}
		}

		public SurfaceHolder getSurfaceHolder() {
			return mSurfaceHolder;
		}

		/* Callback invoked when the surface dimensions change. */
		public void setSurfaceSize(int width, int height) {
			// synchronized to make sure these all change atomically
			synchronized (mSurfaceHolder) {
				colorCatch.setSurfaceSize(width, height);
			}
		}

		/**
		 * Send the game logic to the game to draw.
		 *
		 * @param canvas
		 *            the object to be drawn on.
		 */
		private void doDraw(Canvas canvas) {
			colorCatch.myDraw(canvas);
			canvas.save();
			canvas.restore();
		}

		/**
		 * Send so the game and have it update its game state.
		 */
		private void updatePhysics() {
			colorCatch.updatePhysics();
		}

		/**
		 * If the menu button is pressed and restart is selected the game resets
		 * and returns to the menu.
		 */
		public void returnToMenu() {
			synchronized (mSurfaceHolder) {
				setState(STATE_RUNNING);
			}
			GameVariables.getInstance().resetGame();
			colorCatch.setMenuScreen();
			showStartButton();
			thread.unpause();
		}

		/**
		 * this was removed from the menu.
		 */
		public void doReStart() {
			synchronized (mSurfaceHolder) {
				setState(STATE_RUNNING);
			}
			startGame();
		}

		public void surfaceDestroyed(SurfaceHolder holder) {
			// we have to tell thread to shut down & wait for it to finish, or
			// else
			// it might touch the Surface after we return and explode
			boolean retry = true;
			thread.setRunning(false);
			while (retry) {
				try {
					thread.join();
					retry = false;
				} catch (InterruptedException e) {
				}
			}
		}

		public void musicLink() {
			// TODO Auto-generated method stub

		}
	}
}
