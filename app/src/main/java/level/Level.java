package level;

import android.graphics.Canvas;

public interface Level {
	
	public void myDraw(Canvas canvas);

	public void updatePhysics();
}
