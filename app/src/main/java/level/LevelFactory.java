package level;

public class LevelFactory {
	
	public LevelFactory() {

	}

	public Level createLevel(int levelNumber) {
		switch (levelNumber) {
		case 0:
			return (Level) new LevelEndingImpl();
		case 1:
			return (Level) new Level01Impl();
		case 2:
			return (Level) new Level02Impl();
		case 3:
			return (Level) new Level03Impl();
		case 4:
			return (Level) new Level04Impl();
		case 5:
			return (Level) new Level05Impl();
		case 6:
			return (Level) new Level06Impl();
		case 7:
			return (Level) new Level07Impl();
		case 8:
			return (Level) new Level08Impl();
		case 9:
			return (Level) new Level09Impl();
		case 10:
			return (Level) new Level10Impl();
		}
		return null;
	}
	
}
