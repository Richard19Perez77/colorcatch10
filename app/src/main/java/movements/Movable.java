package movements;

import android.graphics.Point;
import android.graphics.Rect;

public interface Movable {
	
	public void move();

	public Rect getRect();

	public void setRect(Rect rect);

	public void destroy(int right, int top, int left, int bottom, int speed);

	public void setRectBounds(int right, int top, int left, int bottom);

	public void setSpeed(int speed);

	public int getSpeed();

	public void incSpeed();

	public void decSpeed();

	public void changeLateralDirection();

	public void resetDirection();

	public void setNewSideLocation(int left, int top, int right, int bottom);

	public void plotPoints(Point a, Point b);
}
