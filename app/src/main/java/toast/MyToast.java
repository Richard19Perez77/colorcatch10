package toast;

import variables.GameVariables;
import android.app.Activity;
import android.widget.Toast;

/**
 * A class to define the Toast object for messages. I created it as a static
 * class so that it can be called anywhere to display any message needed.
 * 
 * @author Rick
 * 
 */

public class MyToast {

	private static volatile MyToast instance;
	private Activity act;
	private Toast toast;
	private GameVariables gv = GameVariables.getInstance();

	private MyToast() {

	}

	// returns the one object instance
	public static MyToast getInstance() {
		if (instance == null)
			synchronized (GameVariables.class) {
				if (instance == null)
					instance = new MyToast();
			}
		return instance;
	}

	public void createToast() {
		act = (Activity) gv.context;
		act.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				toast = Toast
						.makeText(
								act,
								"Catch squares that are the same color as the border of the player and screen!",
								Toast.LENGTH_LONG);
				toast.show();
			}
		});
	}

	public void createToast(final String message) {
		act = (Activity) gv.context;
		act.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				toast = Toast.makeText(act, message, Toast.LENGTH_LONG);
				toast.show();
			}
		});
	}

	/**
	 * Called if the app closes so it doesn't show the message on the phone menu
	 * after the app closes.
	 */
	public void cancelToast() {
		if (toast != null)
			toast.cancel();
	}

}
