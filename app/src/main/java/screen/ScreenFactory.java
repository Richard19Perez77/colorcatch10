package screen;

/**
 * A class to create the differetn screens the app might use such as loading,
 * menu and game.
 * 
 * @author Rick
 * 
 */

public class ScreenFactory {

	private Screen screen;

	public ScreenFactory() {

	}

	public Screen createScreen(String type) {
		if (type.equals("loading")) {
			screen = new LoadingScreenImpl();
		} else if (type.equals("menu")) {
			screen = new MenuScreenImpl();
		} else if (type.equals("game")) {
			screen = new GameScreenImpl();
		}

		return screen;

	}

}
