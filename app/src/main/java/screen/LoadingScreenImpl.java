package screen;

import background.LoadingScreenBackground;
import android.graphics.Canvas;
import android.view.MotionEvent;

/**
 * A class that defines the logic at the loading screen. Its a simple splash
 * screen.
 * 
 * @author Rick
 * 
 */

public class LoadingScreenImpl implements Screen {

	private LoadingScreenBackground lbg;

	public LoadingScreenImpl() {
		lbg = new LoadingScreenBackground();
	}

	@Override
	public void myDraw(Canvas canvas) {
		lbg.draw(canvas);
	}

	@Override
	public void updatePhysics() {

	}

	@Override
	public void eventAction(MotionEvent event) {

	}
}
