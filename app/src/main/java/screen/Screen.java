package screen;

import android.graphics.Canvas;
import android.view.MotionEvent;

public interface Screen {
	public void myDraw(Canvas canvas);
	public void updatePhysics();
	public void eventAction(MotionEvent event);
}
