package screen;

import background.MenuScreenBackground;
import variables.GameVariables;
import android.graphics.Canvas;
import android.view.MotionEvent;

/**
 * A class that defines the menu screen. A button, some falling squares and
 * itnro text as well as a moving gradient for show.
 * 
 * @author Rick
 * 
 */

public class MenuScreenImpl implements Screen {

	private GameVariables gv;
	private MenuScreenBackground msb;

	public MenuScreenImpl() {
		gv = GameVariables.getInstance();
		msb = new MenuScreenBackground();
	}

	@Override
	public void myDraw(Canvas canvas) {
		msb.draw(canvas);
	}

	@Override
	public void updatePhysics() {
		gv.cyclePaint();
		msb.updatePhysics();
	}

	@Override
	public void eventAction(MotionEvent event) {
		// TODO Auto-generated method stub

	}
}
