package screen;

import audio.Audio;
import background.GameScreenBackground;
import level.Level;
import level.LevelFactory;
import logic.GameLogic;
import variables.GameVariables;

import android.graphics.Canvas;
import android.graphics.Color;
import android.view.MotionEvent;

/**
 * A class to define the Game Screen of color catch.
 *
 * @author Rick Perez
 */

public class GameScreenImpl implements Screen {

    private GameVariables gv;
    private GameLogic gl;
    private GameScreenBackground bg;
    private LevelFactory lf = new LevelFactory();
    private Level level;
    private int colora, colorb;
    private int levels = GameVariables.LEVELS;
    private static final int POINT = 3;

    /**
     * If the game has just continued create the level it left off at. If in debug mode for a particular level skip to it.
     */
    public GameScreenImpl() {
        gv = GameVariables.getInstance();
        bg = new GameScreenBackground();
        gl = new GameLogic();
        lf = new LevelFactory();

        if (gv.continuedGame) {
            gameContinue();
        } else {
            //set debug level
            if (gv.DEBUG_MODE == true) {
                gv.setUpGame();
                level = lf.createLevel(gv.DEBUG_LEVEL);
                gv.setCurrLevel(gv.DEBUG_LEVEL);
            } else {
                level = lf.createLevel(1);
                gv.setCurrLevel(1);
            }
        }

    }

    /**
     * Create the level retrieved from the savedSate database.
     */
    public void gameContinue() {
        // start game with saved game variables loaded
        level = lf.createLevel(gv.getCurrLevel());
    }

    /**
     * Logic across all levels includes drawing the score board and the logic of
     * Scoring. Also keeps track of timing to move to the next level and if the
     * player is out of misses (health).
     */
    @Override
    public void myDraw(Canvas canvas) {

        bg.draw(canvas);

        if (gv.getGameVarsLoaded()) {
            if (gv.getCurrLevel() != 0) {
                canvas.drawRect(0, 0, gv.screenW, gv.screenH,
                        gv.getTargetPaint());

                canvas.drawText(" Score: " + gv.score, 0,
                        gv.whitePaint.getTextSize(), gv.whitePaint);
            }

            level.myDraw(canvas);

            checkColorMatch(canvas);

            // check for new level if not game over
            if (gv.gameTimer == gv.getNewLevel()) {
                gv.setEnemyCreation(false);
                gv.incCurrentLevel();
                // switch to next level if possible
                if (gv.getCurrLevel() <= levels) {
                    gl.destroyAllEnemies(canvas);
                    level = lf.createLevel(gv.getCurrLevel());
                    gv.setEnemyCreation(false);
                    bg.refresh();
                    if (gv.getCurrLevel() == GameVariables.LEVELS / 2)
                        Audio.getInstance().playTrack2();
                }
            }

            // check for 0 health and start game over
            if (gv.getHealth() == 0 && gv.getCurrLevel() != 0) {
                gl.destroyAllEnemies(canvas);
                level = lf.createLevel(0);
                gv.setCurrLevel(0);
                Audio.getInstance().playEndTrack();
            }
        }

    }

    /**
     * Checks the color of the player and the target color to be so similar
     * enough to score a point.
     *
     * @param canvas The object the rest of the objects are drawn on.
     */
    private void checkColorMatch(Canvas canvas) {
        if (gv.player != null && gv.getTargetPaint() != null) {

            colora = colorb = 0;

            if (gv.getTargetPaint().getColor() == Color.RED) {
                colora = gv.player.getPaint().getColor();
                colorb = Color.red(colora);
            } else if (gv.getTargetPaint().getColor() == Color.BLUE) {
                colora = gv.player.getPaint().getColor();
                colorb = Color.blue(colora);
            } else if (gv.getTargetPaint().getColor() == Color.GREEN) {
                colora = gv.player.getPaint().getColor();
                colorb = Color.green(colora);
            }

            if (colorb >= 235 && colorb <= 256) {
                Audio.getInstance().playSound(POINT);
                gl.destroyAllEnemies(canvas);
                gl.newTargetColor();
                gv.score = gv.score + GameVariables.SCORE_INC;

                //reset bg shapes to white
                gv.mixedColorInt = Color.WHITE;
                gv.setNewMixedColorInt = true;
            }
        }
    }

    /**
     * If the game is playing a level then increment the game timer. Updates
     * game physics.
     */
    @Override
    public void updatePhysics() {
        //check for new color to background shapes
        if (gv.setNewMixedColorInt == true)
            bg.updateColors(gv.mixedColorInt);

        gv.cyclePaint();
        if (gv.getCurrLevel() != 0)
            gv.gameTimer = gv.gameTimer + 1;
        bg.updatePhysics();
        level.updatePhysics();
    }

    /**
     * Called to send the motion event to the current level for specific level
     * logic.
     */
    @Override
    public void eventAction(MotionEvent event) {
        // ending screen graphics are touch responsive
        if (gv.getCurrLevel() == 0) {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                gv.clearXsYs();
                gv.enemyCount = 0;
            }

            if (gv.getEnemyCount() < gv.getLevelEnemies()) {
                gv.addEndX(gv.getNewX());
                gv.addEndY(gv.getNewY());
                gv.incEnemyCount();
            } else {
                gv.removeEndXY();
                gv.decEnemyCount();
            }
        }
    }
}
