package game;

import audio.Audio;
import screen.Screen;
import screen.ScreenFactory;
import variables.GameVariables;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.MotionEvent;

/**
 * A class that defines how ColorCatch is played.
 * 
 * @author Rick
 * 
 */

public class ColorMixerGameImpl implements Game {

	/**
	 * Game Variables are used to store date in a static class that any class
	 * can access.
	 */
	private GameVariables gv = GameVariables.getInstance();
	/**
	 * The Screen Factory creates the loading, menu or game screen.
	 */
	private ScreenFactory screenFactory = new ScreenFactory();
	/**
	 * The current game screen.
	 */
	private Screen screen;

	/**
	 * On start the game goes to the loading screen.
	 * 
	 */
	public ColorMixerGameImpl() {
		setLoadingScreen();
	}

	@Override
	public void myDraw(Canvas canvas) {
		screen.myDraw(canvas);
	}

	@Override
	public void updatePhysics() {
		screen.updatePhysics();
	}

	@Override
	public boolean getMenuPlaying() {
		return gv.getMenuPlaying();
	}

	@Override
	public void setSoundStopped(int currentPosition) {
		Audio.getInstance().stoppedAt = currentPosition;
	}

	@Override
	public void startGame() {
		gv.setGamePlaying(true);
		gv.loadingPlaying = false;
		gv.setMenuPlaying(false);
		screen = screenFactory.createScreen("game");
		Audio.getInstance().playGame();
	}

	@Override
	public boolean getLoadingPlaying() {
		return gv.loadingPlaying;
	}

	@Override
	public void setLoadingPlaying(boolean loadingPlaying) {
		gv.loadingPlaying = loadingPlaying;

	}

	@Override
	public void setMenuPlaying(boolean menuPlaying) {
		gv.setMenuPlaying(menuPlaying);
	}

	@Override
	public boolean getGameVarsLoaded() {
		return gv.getGameVarsLoaded();
	}

	/**
	 * I added setting the touch coordinates here so that I avoid any game logic
	 * in the View or Activity class.
	 */
	@Override
	public void setNewOnTouchCoords(int x, int y) {
		gv.newX = x;
		gv.newY = y;
	}

	@Override
	public void eventAction(MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_DOWN) {
			gv.movePlayerToPoint();
		}

		if (event.getAction() == MotionEvent.ACTION_UP) {

		}

		if (event.getAction() == MotionEvent.ACTION_MOVE) {
			gv.movePlayerToPoint();
		}
		// screen specific action events, such as end game drawing for fun.
		screen.eventAction(event);

	}

	@Override
	public void init(DisplayMetrics displayMetrics, Resources resources) {
		gv.setMetrics(displayMetrics);
		gv.res = resources;
	}

	@Override
	public void setSurfaceSize(int width, int height) {
		gv.screenW = width;
		gv.screenH = height;
		gv.setScreenVarsSizes();
	}

	@Override
	public void setMenuScreen() {
		gv.setGamePlaying(false);
		gv.loadingPlaying = false;
		gv.setMenuPlaying(true);
		screen = screenFactory.createScreen("menu");
		Audio.getInstance().playMenu();
	}

	@Override
	public void setLoadingScreen() {
		gv.setGamePlaying(false);
		gv.loadingPlaying = true;
		gv.setMenuPlaying(false);
		screen = screenFactory.createScreen("loading");
	}
}
