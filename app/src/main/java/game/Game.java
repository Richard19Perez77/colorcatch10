package game;

import android.content.res.Resources;
import android.graphics.Canvas;
import android.util.DisplayMetrics;
import android.view.MotionEvent;

public interface Game {
	public void myDraw(Canvas canvas);
	public void updatePhysics();
	public boolean getMenuPlaying();
	public void setSoundStopped(int currentPosition);
	public void startGame();
	public boolean getLoadingPlaying();
	public void setLoadingPlaying(boolean b);
	public void setMenuPlaying(boolean b);
	public void setMenuScreen();
	public void setLoadingScreen();
	public boolean getGameVarsLoaded();
	public void setNewOnTouchCoords(int x, int y);
	public void eventAction(MotionEvent event);
	public void init(DisplayMetrics displayMetrics, Resources resources);
	public void setSurfaceSize(int width, int height);
}
