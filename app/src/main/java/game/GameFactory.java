package game;

/**
 * A class that creates the game. Can be extended.
 * 
 * @author Rick
 * 
 */

public class GameFactory {

	public GameFactory() {

	}

	public Game createGame(int gameNumber) {
		switch (gameNumber) {
		case 1:
			return (Game) new ColorMixerGameImpl();
		default:
			return null;
		}
	}

}
