package audio;

import java.io.IOException;

import radical.appwards.colorcatch.R;
import variables.GameVariables;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.util.SparseIntArray;

/**
 * A class that will run the logic for the sounds in the game. I made this a
 * static class so that the audio can be changed and recored anywhere in the
 * app. I also use it to get the app context anywhere which is very handy when
 * in classes not from the main activity.
 * 
 * @author Rick
 * 
 */

public class Audio implements MediaPlayer.OnPreparedListener {

	// create one var of the type it is.
	private volatile static Audio instance;
	public MediaPlayer mp;
	public SoundPool sp;
	public AudioManager audioManager;
	public float actualVolume, maxVolume, volume, fSpeed, streamVolumeCurrent,
			streamVolumeMax, prevVolume = 1, currVolume = 1;
	public SparseIntArray soundsMap;
	public AudioManager mgr;
	public static final int HIT = 1, MISS = 2, POINT = 3;
	public int stoppedAt;
	public boolean musicOn = true, soundOn = true, musicResumed;
	public Uri path;
	public int level;
	public boolean resuming;
	GameVariables gv = GameVariables.getInstance();

	/**
	 * Singleton objects needs private constructor that will help ensure the
	 * class is created outside of the class.
	 */
	private Audio() {
	}

	/**
	 * Returns the one object instance that is created on this first call
	 * 
	 * @return The only Audio instance.
	 */
	public static Audio getInstance() {
		if (instance == null)
			synchronized (Audio.class) {
				if (instance == null)
					instance = new Audio();
			}
		return instance;
	}

	public void setSoundStopped() {
		stoppedAt = mp.getCurrentPosition();
	}

	/**
	 * musicResumed is used to flag when resuming the game so it doesn't start
	 * the track over at game continue.
	 */
	public void playMenu() {
		if (!musicResumed) {
			try {
				path = Uri
						.parse("android.resource://radical.appwards.colorcatch/"
								+ R.raw.intro);
				mp.reset();
				mp.setDataSource(gv.context, path);
				mp.setOnPreparedListener(this);
				mp.setLooping(true);
				mp.setVolume(currVolume, currVolume);
				mp.setOnPreparedListener(this);
				mp.prepareAsync();
			} catch (IllegalStateException | IllegalArgumentException | IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Called to toggle music on/off.
	 */
	public void toggleMusic() {
		if (musicOn) {
			musicOn = false;
			prevVolume = currVolume;
			currVolume = 0;
			mp.setVolume(currVolume, currVolume);
		} else {
			musicOn = true;
			currVolume = prevVolume;
			mp.setVolume(currVolume, currVolume);
		}
	}

	/**
	 * Used to toggle sound effects on/off.
	 */
	public void toggleSound() {
		if (soundOn) {
			soundOn = false;
		} else {
			soundOn = true;
		}
	}

	/**
	 * When the app is resumed the level is used to tell what track to start and
	 * the soundStopped at should be set from the database.
	 */
	public void resumeMusic() {

		level = GameVariables.getInstance().level;

		if (level <= GameVariables.LEVELS / 2)
			path = Uri.parse("android.resource://radical.appwards.colorcatch/"
					+ R.raw.track1);
		else
			path = Uri.parse("android.resource://radical.appwards.colorcatch/"
					+ R.raw.track2);

		try {
			mp.reset();
			mp.setDataSource(gv.context, path);
			mp.setOnPreparedListener(this);
			mp.setLooping(true);
			mp.setVolume(1, 1);
			resuming = true;
			mp.setOnPreparedListener(this);
			mp.prepareAsync();
			musicResumed = true;
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Used to load and start the media player. If not done here it will be done
	 * on the main game thread and it will cause about a 1 second hiccup.
	 */
	@Override
	public void onPrepared(MediaPlayer mp) {
		if (resuming)
			mp.seekTo(stoppedAt);
		resuming = false;
		if (musicOn)
			mp.start();
	}

	/**
	 * If the game is not resumed from a closed app instance it will play the
	 * track from the beginning.
	 */
	public void playGame() {
		if (!musicResumed) {
			try {
				path = Uri
						.parse("android.resource://radical.appwards.colorcatch/"
								+ R.raw.track1);
				mp.reset();
				mp.setDataSource(gv.context, path);
				mp.setOnPreparedListener(this);
				mp.setLooping(true);
				mp.setVolume(currVolume, currVolume);
				mp.setOnPreparedListener(this);
				mp.prepareAsync();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		musicResumed = false;
	}

	/**
	 * Played at the middle of the game.
	 */
	public void playTrack2() {
		try {
			path = Uri.parse("android.resource://radical.appwards.colorcatch/"
					+ R.raw.track2);
			mp.reset();
			mp.setDataSource(gv.context, path);
			mp.setOnPreparedListener(this);
			mp.setLooping(true);
			mp.setVolume(currVolume, currVolume);
			mp.setOnPreparedListener(this);
			mp.prepareAsync();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Played in the ending score screen.
	 */
	public void playEndTrack() {
		try {
			path = Uri.parse("android.resource://radical.appwards.colorcatch/"
					+ R.raw.ending);
			mp.reset();
			mp.setDataSource(gv.context, path);
			mp.setOnPreparedListener(this);
			mp.setLooping(true);
			mp.setVolume(currVolume, currVolume);
			mp.setOnPreparedListener(this);
			mp.prepareAsync();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Plays the sound at the appropriate volume relative to the current phone
	 * volume.
	 * 
	 * @param sound
	 *            The mapping of the sound to differentiate what to play.
	 */
	public void playSound(int sound) {
		// plays the sounds effect called
		mgr = (AudioManager) gv.context.getSystemService(Context.AUDIO_SERVICE);
		streamVolumeCurrent = mgr.getStreamVolume(AudioManager.STREAM_MUSIC);
		streamVolumeMax = mgr.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		if (soundOn) {
			volume = streamVolumeCurrent / streamVolumeMax;
			volume /= 2;
		} else
			volume = 0;
		sp.play(soundsMap.get(sound), volume, volume, 1, 0, fSpeed);
	}

	/**
	 * Release memory associated with sound pool.
	 */
	public void releaseSoundPool() {
		if (sp != null) {
			sp.release();
			sp = null;
		}
	}

	public void resumeAudio() {
		audioManager = (AudioManager) gv.context
				.getSystemService(Context.AUDIO_SERVICE);
		actualVolume = (float) audioManager
				.getStreamVolume(AudioManager.STREAM_MUSIC);
		maxVolume = (float) audioManager
				.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
		volume = actualVolume / maxVolume;

		fSpeed = 1.0f;
		soundsMap = new SparseIntArray();
		soundsMap.put(HIT, sp.load(gv.context, R.raw.hit, 1));
		soundsMap.put(MISS, sp.load(gv.context, R.raw.miss, 1));
		soundsMap.put(POINT, sp.load(gv.context, R.raw.point, 1));

		if (stoppedAt > 0) {
			resumeMusic();
		}
	}
}