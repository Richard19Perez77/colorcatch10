package background;

import java.util.Random;

import variables.GameVariables;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RadialGradient;

/**
 * A class to draw the menu screen. In each iteration of draw and update physics
 * I create a new gradient that increases in size to a point and reduces in size
 * to a point. Adding touch options here seems to slow down my poor phone so
 * maybye there is a better way to do this... Threading I am looking at you!
 * 
 * @author Rick
 * 
 */

public class MenuScreenBackground {

	private GameVariables gv;
	private Paint circlePaint = new Paint(), yellowPaint = new Paint(),
			greenPaint = new Paint(), blackPaint = new Paint();
	private RadialGradient radialG;
	private String radical, music;
	private int x, y, startR, endR, r, colora, colorb, squares, slen, h, w;
	private float[] xs, ys;
	private Random rand = new Random();
	private boolean menuVarsLoaded, incR;

	public MenuScreenBackground() {
		gv = GameVariables.getInstance();

		menuVarsLoaded = false;

		Thread loadingThread = new Thread() {
			@Override
			public void run() {
				initMenuObjects();
			}

			private void initMenuObjects() {

				w = gv.screenW;
				h = gv.screenH;
				squares = (h * w) / 10000;
				xs = new float[squares];
				ys = new float[squares];

				greenPaint.setColor(Color.GREEN);
				greenPaint.setStrokeWidth(3);
				greenPaint.setStyle(Style.STROKE);
				yellowPaint.setColor(Color.YELLOW);
				yellowPaint.setStrokeWidth(3);
				yellowPaint.setStyle(Style.STROKE);
				blackPaint.setColor(Color.BLACK);
				blackPaint.setStrokeWidth(3);
				blackPaint.setStyle(Style.STROKE);
				radical = "Radical\u2605Appwards";
				music = "\u2669pinklogik.bandcamp.com\u2669";
				x = w / 2;
				y = h / 2;
				startR = r = h / 4;
				endR = r * 4;
				colora = Color.RED;
				colorb = Color.BLUE;
				radialG = new RadialGradient(x, y, r, colora, colorb,
						android.graphics.Shader.TileMode.CLAMP);
				circlePaint.setShader(radialG);

				for (int i = 0; i < squares; i++) {
					xs[i] = rand.nextInt(w);
				}

				for (int i = 0; i < squares; i++) {
					ys[i] = rand.nextInt(h);
				}
				slen = h / 40;
				menuVarsLoaded = true;
			}
		};

		loadingThread.start();
	}

	public void draw(Canvas canvas) {
		canvas.drawColor(Color.BLACK);
		if (menuVarsLoaded) {
			canvas.drawCircle(x, y, endR, circlePaint);

			canvas.drawText(radical, w / 2, h / 10, gv.blackPaint);
			canvas.drawText(radical, w / 2, h / 10 + 3, gv.randPaint);

			canvas.drawText(music, w / 2, h - h / 10, gv.blackPaint);
			canvas.drawText(music, w / 2, h - h / 10 - 3, gv.randPaint);

			canvas.drawText("Color", w / 2, h / 3, gv.getBlackBoldPaint());
			canvas.drawText("Color", w / 2, h / 3 + 5, gv.getYellowBoldPaint());

			for (int i = 0; i < squares; i++) {
				canvas.drawRect(xs[i], ys[i], xs[i] + slen, ys[i] + slen,
						blackPaint);
				canvas.drawRect(xs[i], ys[i] + 4, xs[i] + slen, ys[i] + slen
						+ 4, greenPaint);
			}

			canvas.drawText("Catch", w / 2, h - h / 4, gv.getBlackBoldPaint());
			canvas.drawText("Catch", w / 2, h - h / 4 - 5,
					gv.getYellowBoldPaint());
		}

	}

	public void updatePhysics() {
		if (menuVarsLoaded) {
			for (int i = 0; i < squares; i++) {
				ys[i] = ys[i] + 1;
				if (ys[i] > h)
					ys[i] = -1 * (h / 20);
			}

			if (r < startR)
				incR = true;

			if (r > endR)
				incR = false;
			// 4 works well here also
			if (incR) {
				r += 7;
			} else {
				r -= 7;
			}

			radialG = new RadialGradient(x, y, r, colora, colorb,
					android.graphics.Shader.TileMode.CLAMP);
			circlePaint.setShader(radialG);
		}
	}
}
