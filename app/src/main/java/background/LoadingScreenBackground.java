package background;

import variables.GameVariables;
import android.graphics.Canvas;
import android.graphics.Color;

/**
 * A class to create the loading screen background. I use the game variables to
 * get the screen size.
 * 
 * @author Rick
 * 
 */

public class LoadingScreenBackground {

	private GameVariables gv;

	public LoadingScreenBackground() {
		gv = GameVariables.getInstance();
	}

	public void draw(Canvas canvas) {
		canvas.drawColor(Color.RED);

		canvas.drawText("Color", gv.screenW / 2, gv.screenH / 3 + 5,
				gv.getYellowBoldPaint());

		canvas.drawText("Catch", gv.screenW / 2,
				gv.screenH - gv.screenH / 4 - 5,
				gv.getYellowBoldPaint());
	}

}
