package colors;

/**
 * A class to define colors that I planned to use but instead used three basic
 * colors (RGB )and the default grey colors. Could be used though in new levels.
 * The mixing of colors hasn't been tested yet.
 * 
 * @author Rick
 * 
 */

public class Colors {

	String[] colors;

	public Colors() {
		colors = new String[6];
		initColors();
	}

	private void initColors() {
		// red
		colors[0] = "FF0000";
		// blue
		colors[1] = "0000FF";
		// green
		colors[2] = "00FF00";
		// yellow
		colors[3] = "FFFF00";
		// purple
		colors[4] = "FF00FF";
		// teal
		colors[5] = "FF00FF";
		// grey
		colors[6] = "808080";
	}

	public String mixColors(String a, String b) {
		// check for red mixes
		if (a.equals("FF0000")) {
			redMixes(a);
		}
		//check for blue mixes
		if (a.equals("0000FF")) {
			blueMixes(a);
		}
		return null;
	}

	private String redMixes(String b) {
		if (b.equals("0000FF")) {
			// red and blue make purple
			return "FF00FF";
		} else if (b.equals("00FF00")) {
			// red and green make yellow
			return "FFFF00";
		} else if (b.equals("00FF00")) {
			// red and yellow make orange
			return "FF8000";
		} else if (b.equals("00FF00")) {
			// red and purple make fuscia
			return "FF0080";
		} else if (b.equals("00FF00")) {
			// red and teal make grey
			return "808080";
		} else {
			return "FFFFFF";
		}
	}

	private String blueMixes(String b) {
		if (b.equals("0000FF")) {
			// blue and red make purple
			return "FF00FF";
		} else if (b.equals("00FF00")) {
			// blue and green make teal
			return "00FFFF";
		} else if (b.equals("00FF00")) {
			// blue and yellow make grey
			return "808080";
		} else if (b.equals("00FF00")) {
			// blue and purple make violet
			return "8000FF";
		} else if (b.equals("00FF00")) {
			// blue and teal make grey
			return "808080";
		} else {
			return "FFFFFF";
		}
	}
}
